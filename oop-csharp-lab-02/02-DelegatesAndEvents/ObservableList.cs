﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DelegatesAndEvents {

    public class ObservableList<TItem> : IObservableList<TItem>
    {
        List<TItem> l = new List<TItem>();
        public IEnumerator<TItem> GetEnumerator()
        {
            return l.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(TItem item)
        {
            l.Add(item);
        }

        public void Clear()
        {
            l.Clear();
        }

        public bool Contains(TItem item)
        {
            return l.Contains(item);
        }

        public void CopyTo(TItem[] array, int arrayIndex)
        {
           //TODO
        }

        public bool Remove(TItem item)
        {
            return l.Remove(item);
        }

        public int Count
        {
            get
            {
                return l.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                if (l.AsReadOnly().GetHashCode().Equals(l.GetHashCode()))
                {
                    return true;
                }
                return false;
            }
        }
        public int IndexOf(TItem item)
        {
            return l.IndexOf(item);
        }

        public void Insert(int index, TItem item)
        {
            TItem[] a = l.ToArray();
            a[index] = item;
            l = new List<TItem>(a);
        }

        public void RemoveAt(int index)
        {
            l.RemoveAt(index);
        }

        public TItem this[int index]
        {
            get { return l[index]; }
            set { l[index] = value; }
        }

        public event ListChangeCallback<TItem> ElementInserted;
        public event ListChangeCallback<TItem> ElementRemoved;
        public event ListElementChangeCallback<TItem> ElementChanged;

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}